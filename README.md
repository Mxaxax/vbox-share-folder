## INTRODUCTION
Ce guide vous explique comment configurer des dossiers partagés entre une machine hôte et une machine invitée dans VirtualBox. Cette configuration permet de partager des fichiers entre les deux machines de manière transparente et facilite le développement et les tests de logiciels.

## Processus

1. Ouvrez VirtualBox.

2. Faites un clic droit sur votre machine virtuelle, puis cliquez sur Paramètres.

3. Allez dans la section Dossiers partagés.

4. Ajoutez un nouveau dossier partagé.

5. Dans la fenêtre Ajouter un partage, sélectionnez le chemin du dossier sur votre hôte que vous voulez rendre accessible dans votre machine virtuelle.

6. Dans le champ Nom du dossier, tapez "shared".

7. Décochez les options Lecture seule et Montage automatique, et cochez l'option Permanent.

8. Démarrez votre machine virtuelle.

9. Une fois que votre machine virtuelle est démarrée, allez dans le menu Périphériques -> Insérer l'image CD des Additions invité.

10. Utilisez la commande suivante pour monter le CD :
```bash
sudo mount /dev/cdrom /media/cdrom
```
11. Installez les dépendances pour les Additions invité VirtualBox :
```bash
sudo apt-get update
```
```bash
sudo apt-get install build-essential linux-headers-`uname -r
```
12. Exécutez le script d'installation pour les Additions invité :
```bash
sudo /media/cdrom/./VBoxLinuxAdditions.run
```
13. Redémarrez la machine virtuelle :
```bash
sudo shutdown -r now
```
14. Créez un dossier "shared" dans votre répertoire personnel :
```bash
mkdir ~/shared
```
15. Montez le dossier partagé de l'hôte sur votre répertoire ~/shared :
```bash
sudo mount -t vboxsf shared ~/shared
```
16. Le dossier de l'hôte devrait maintenant être accessible dans la machine virtuelle.
```bash
cd ~/shared
```
## IMPORTANT : Rendez le montage du dossier permanent
Ce montage de dossier est temporaire et il disparaîtra au prochain redémarrage.

Pour le rendre permanent, nous allons configurer le système de sorte qu'il monte le répertoire ~/shared au démarrage.

1. Éditez le fichier fstab dans le répertoire /etc :
```bash
sudo nano /etc/fstab
```
2. Ajoutez la ligne suivante à fstab (séparées par des tabulations) et appuyez sur Ctrl+O pour Enregistrer.
```bash
shared	/home/<username>/shared	vboxsf	defaults	0	0
```
3. Éditez les modules :
```bash
sudo nano /etc/modules
```
4. Ajoutez la ligne suivante à /etc/modules et enregistrez :
```bash
vboxsf
```
5. Redémarrez la machine virtuelle et connectez-vous à nouveau.
```bash
shutdown -r now
```
6. Allez dans votre répertoire personnel et vérifiez si le fichier est surligné en vert.
```bash
ls ~/
```
Si c'est le cas, félicitations ! Vous avez réussi à lier le répertoire dans votre machine virtuelle avec votre dossier hôte.
